import os
import can

os.system('sudo ip link set can0 type can bitrate 100000')
os.system('sudo ifconfig can0 up')

can0 = can.interface.Bus(channel = 'can0', bustype = 'socketcan_ctypes')# socketcan_native

msg = can.Message(arbitration_id=0x124, data=[0, 1, 0, 0, 0, 0, 0, 0], extended_id=False)
can0.send(msg)

os.system('sudo ifconfig can0 down')