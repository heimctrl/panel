#!/bin/bash

#############
# install nodejs
#############

curl -sL https://deb.nodesource.com/setup_12.x | sudo bash -
sudo apt-get install -y nodejs

#############
# turn off WIFI (assumes wired LAN is connected)
#############


#############
# configure system
# language, keyboard
#############


#############
# enable CAN hat
# https://www.waveshare.com/w/upload/2/29/RS485-CAN-HAT-user-manuakl-en.pdf
# https://www.waveshare.com/wiki/Libraries_Installation_for_RPi
#############

# install wiringPi
sudo apt-get install wiringpi

# install BCM2835 C library
wget http://www.airspayce.com/mikem/bcm2835/bcm2835-1.64.tar.gz
tar zxvf bcm2835-1.64.tar.gz
cd bcm2835-1.64
./configure
make
sudo make check
sudo make install
cd ..

sudo apt-get install -y i2c-tools

# install python packages
pip install RPi.GPIO spidev

# install smbus library
sudo apt-get install -y python-smbus python-serial

# install python can libraries
pip install python-can

# append to /boot/config.txt
# dtoverlay=mcp2515-can0,oscillator=8000000,interrupt=25,spimaxfrequency=1000000
# check for success:
#   dmesg | grep -i '\(can\|spi\)'
#   mcp251x spi0.0 can0: MCP2515 successfully initialized.

#############
# setup kiosk
#############

 